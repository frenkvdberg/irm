#!/bin/bash 
# Author: Frank van den Berg, s2663201
# run as: ./extract_guilty_sentences.sh data_all_ages.csv data_65_and_older.csv

CSV1=$1
CSV2=$2

echo "- Age category: all ages -"

echo "Amount of people pleaded guilty with sentence in 2017:"
SENTENCE1=`tail -n +2 $CSV1 | grep 'Schuldigverklaring met straf' | cut -f15 -d";" | sed 's/"//g' | sed 's/;/ + /g' | tr -d $'\r'`
echo $SENTENCE1
echo "Amount of people pleaded guilty without sentence in 2017:"
NOSENTENCE1=`tail -n +2 $CSV1 | grep 'Schuldigverklaring zonder straf' | cut -f15 -d";" | sed 's/"//g' | sed 's/;/ + /g' | tr -d $'\r'`
echo $NOSENTENCE1
echo "Total amount of people pleaded guilty in 2017:"
TOTAL1=`echo $SENTENCE1 + $NOSENTENCE1 | bc`
echo $TOTAL1



echo ""
echo "- Age category: only 65 years and older -"

echo "Amount of people pleaded guilty with sentence in 2017:"
SENTENCE2=`tail -n +2 $CSV2 | grep 'Schuldigverklaring met straf' | cut -f15 -d";" | sed 's/"//g' | sed 's/;/ + /g' | tr -d $'\r'`
echo $SENTENCE2
echo "Amount of people pleaded guilty without sentence in 2017:"
NOSENTENCE2=`tail -n +2 $CSV2 | grep 'Schuldigverklaring zonder straf' | cut -f15 -d";" | sed 's/"//g' | sed 's/;/ + /g' | tr -d $'\r'`
echo $NOSENTENCE2
echo "Total amount of people pleaded guilty in 2017:"
TOTAL2=`echo $SENTENCE2 + $NOSENTENCE2 | bc`
echo $TOTAL2



echo ""
echo "- Age category: only younger than 65 years -"

echo "Amount of people pleaded guilty with sentence in 2017:"
SENTENCE3=`echo $SENTENCE1 - $SENTENCE2 | bc`
echo $SENTENCE3
echo "Amount of people pleaded guilty without sentence in 2017:"
NOSENTENCE3=`echo $NOSENTENCE1 - $NOSENTENCE2 | bc`
echo $NOSENTENCE3
echo "Total amount of people pleaded guilty in 2017:"
TOTAL3=`echo $TOTAL1 - $TOTAL2 | bc`
echo $TOTAL3

