README for using the shellscript extract_guilty_sentences.sh.

________________________
Obtaining the data:

To use the shellscript, two CSV files are needed.
First, go to https://opendata.cbs.nl/statline/#/CBS/nl/dataset/83944NED/table?ts=1552389906357.
(Should the link not work, then google "Vervolging en berechting misdrijven; persoonsgegevens", it will pop up as the first result.)

On this page, click on the download button which is located on the upper-right side of the page to download the first file.
This first file contains data about all the age categories, so we save it as "data_all_ages.csv".
For the second file, click on the age filtering option at the left side of the page, it is labeled as "leeftijd".
In the menu that pops up, click on the category "65 jaar en ouder" to filter by age category 65 years and older.
Now download this file and save it as "data_65_and_older.csv".


________________________
Running the shellscript:

The shellscript should be run as:
$ ./extract_guilty_sentences.sh data_all_ages.csv data_65_and_older.csv

!! Note that data_all_ages.csv should be the first argument and data_65_and_older.csv should be the second, otherwise the script won't give the proper results.

By using grep and cut commands as well as basic math, the shellscript will extract only the relevant numbers for our research from the two csv files.

The results will be printed in the terminal. 



